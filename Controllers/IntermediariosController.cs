using Microsoft.AspNetCore.Mvc;

namespace Humano.Controllers;

[ApiController]
[Route("[controller]")]
public class IntermediariosController : ControllerBase
{
    private static readonly string[] Licencias = new[]
    {
        "HBC", "ABC", "SQSX", "SAQ", "BAX", "SDB", "OKL", "JAQ", "MPQ", "BIQ"
    };

    private readonly ILogger<IntermediariosController> _logger;

    public IntermediariosController(ILogger<IntermediariosController> logger)
    {
        _logger = logger;
    }

    [Route("LicenciasProximaVencimiento")]
    [HttpGet]
    public IEnumerable<LicenciaIntermediario> Get()
    {
        return Enumerable.Range(1, 5).Select(index => new LicenciaIntermediario
        {
            CodigoIntermediario = Random.Shared.Next(1100, 3000),
            FechaVencimiento = DateTime.Now.AddDays(Random.Shared.Next(1, 59)),
            Licencia = Licencias[Random.Shared.Next(Licencias.Length)]+"-"+Random.Shared.Next(Licencias.Length)
        })
        .ToArray();
    }
}
