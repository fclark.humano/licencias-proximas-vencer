namespace Humano;

public class LicenciaIntermediario
{
    public int CodigoIntermediario {get; set;}
    public DateTime FechaVencimiento { get; set; }
    public string? Licencia { get; set; }
}
